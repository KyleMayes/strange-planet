#!/usr/bin/env python

import pygal
import subprocess
import sys

SAMPLES = 512

slower = int(sys.argv[1])
supper = int(sys.argv[2])
plower = int(sys.argv[3])
pupper = int(sys.argv[4])

def sample(call):
    output = subprocess.check_output(call)
    return float(output[output.find(' in ') + 4:-3])

def benchmark(species, population):
    print '{}, {}...'.format(species, population)
    executable = './target/release/benchmark-strange-planet'
    call = [executable, str(species), str(population), str(SAMPLES)]
    output = subprocess.check_output(call)
    return float(output[:output.find('ns')]) / 1000.0

def collect(species):
    return map(lambda p: benchmark(species, p), xrange(plower, pupper + 1))

benchmarks = map(lambda s: (s, collect(s)), xrange(slower, supper + 1))

chart = pygal.Line()
chart.style = pygal.style.CleanStyle
chart.title = 'Strange Planet (average of {} samples)'.format(SAMPLES)
chart.x_title = 'Population'
chart.y_title = 'Microseconds'
chart.x_labels = map(str, xrange(plower, pupper + 1))

for (species, benchmarks) in benchmarks:
    chart.add('{} species'.format(species), benchmarks)

chart.render_to_file('benchmark_{}_{}_{}_{}.svg'.format(slower, supper, plower, pupper))
