extern crate clap;
extern crate strange_planet;
extern crate time;

use clap::{App, Arg};

fn is_usize(value: String) -> Result<(), String> {
    value.parse::<usize>().map(|_| ()).map_err(|e| format!("{} ({:?})", e, value))
}

fn sample(species: usize, creatures: usize) -> u64 {
    let start = time::precise_time_ns();
    strange_planet::generate(species, creatures);
    time::precise_time_ns() - start
}

fn main() {
    // Define the executable argument specification.
    let matches = App::new("benchmark-strange-planet")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Kyle Mayes <kyle@mayeses.com>")
        .about("Benchmarks strange-planet generation.")
        .arg(Arg::with_name("species")
            .help("The number of species")
            .required(true)
            .validator(is_usize))
        .arg(Arg::with_name("creatures")
            .help("The number of creatures")
            .required(true)
            .validator(is_usize))
        .arg(Arg::with_name("samples")
            .help("The number of timing samples to take")
            .required(true)
            .validator(is_usize))
        .get_matches();

    // Extract the executable arguments.
    let species = matches.value_of("species").unwrap().parse::<usize>().unwrap();
    let creatures = matches.value_of("creatures").unwrap().parse::<usize>().unwrap();
    let samples = matches.value_of("samples").unwrap().parse::<usize>().unwrap();

    // Benchmark strange planet generation.
    let sum = (0..samples).map(|_| sample(species, creatures)).sum::<u64>();
    let average = sum as f64 / samples as f64;
    println!("{}ns", average);
}
