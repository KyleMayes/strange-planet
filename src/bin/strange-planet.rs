extern crate clap;
extern crate strange_planet;
extern crate time;

use std::fs::{File};
use std::io::{Write};
use std::process::{Command};

use clap::{App, Arg};

fn is_usize(value: String) -> Result<(), String> {
    value.parse::<usize>().map(|_| ()).map_err(|e| format!("{} ({:?})", e, value))
}

fn time<T, F>(name: &str, mut f: F) -> T where F: FnMut() -> T {
    let start = time::precise_time_ns();
    let result = f();
    let elapsed = (time::precise_time_ns() - start) as f64 / 1000000.0;
    println!("{} in {}ms", name, elapsed);
    result
}

fn main() {
    // Define the executable argument specification.
    let matches = App::new("strange-planet")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Kyle Mayes <kyle@mayeses.com>")
        .about("Generates strange planet NFAs.")
        .arg(Arg::with_name("species")
            .help("The number of species")
            .required(true)
            .validator(is_usize))
        .arg(Arg::with_name("creatures")
            .help("The number of creatures")
            .required(true)
            .validator(is_usize))
        .arg(Arg::with_name("svg")
            .help("Sets whether an SVG will be generated")
            .long("svg"))
        .get_matches();

    // Extract the executable arguments.
    let species = matches.value_of("species").unwrap().parse::<usize>().unwrap();
    let creatures = matches.value_of("creatures").unwrap().parse::<usize>().unwrap();

    // Generate the DOT file.
    let planet = time("Generated strange planet", || strange_planet::generate(species, creatures));
    File::create("nfa.dot").unwrap().write(planet.to_dot().as_bytes()).unwrap();

    // Generate the SVG file.
    if matches.is_present("svg") {
        let arguments = &["-Tsvg", "nfa.dot", "-o", "nfa.svg"];
        time("Rendered SVG", || Command::new("dot").args(arguments).status().unwrap());
    }
}
