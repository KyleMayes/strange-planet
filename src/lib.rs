#![feature(conservative_impl_trait)]

extern crate smallvec;

use std::cmp;
use std::ops;
use std::collections::{HashMap, HashSet};

use smallvec::{SmallVec};

mod set;
use self::set::{IndexSet};

/// A type used to store species population counts.
type Counts = SmallVec<[usize; 8]>;

/// Builds a list of species population counts.
macro_rules! counts {
    ($($count:expr), *) => ({
        let mut counts = Counts::new();
        $(counts.push($count);)*
        counts
    });
}

//================================================
// Enums
//================================================

// Failure _______________________________________

/// Indicates the failure possibility of a strange planet.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Failure {
    /// The strange planet must fail.
    Guaranteed,
    /// The strange planet might fail.
    Possible,
    /// The strange planet cannot fail.
    Impossible,
}

//================================================
// Structs
//================================================

// State _________________________________________

/// A state in a strange planet NFA.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct State {
    /// The name of this state.
    pub name: String,
    /// The species population counts of this state in descending order.
    pub counts: Counts,
    /// The indices of the states this state can transition into.
    pub forward: IndexSet,
    /// The indices of the states that can transition into this state.
    pub back: IndexSet,
    /// The failure possibility of the strange planet if it is in this state.
    pub failure: Failure,
}

impl State {
    /// Constructs a new `State`.
    fn new(counts: Counts) -> State {
        let name = counts.iter().map(ToString::to_string).collect::<Vec<_>>().join("-");
        State {
            name: name,
            counts: counts,
            forward: IndexSet::new(),
            back: IndexSet::new(),
            failure: Failure::Impossible,
        }
    }

    /// Returns an iterator over the states this state can transition into.
    fn forward<'p>(&'p self, planet: &'p StrangePlanet) -> impl Iterator<Item=&'p State> + 'p {
        self.forward.iter().map(move |i| &planet[*i])
    }
}

impl ops::Index<usize> for State {
    type Output = usize;

    fn index(&self, index: usize) -> &usize {
        &self.counts[index]
    }
}

// StrangePlanet _________________________________

/// A strange planet NFA.
#[derive(Clone, Debug)]
pub struct StrangePlanet {
    /// The possible states of this strange planet NFA.
    pub states: Vec<State>,
}

impl StrangePlanet {
    /// Constructs a new `StrangePlanet`.
    pub fn new(counts: Vec<Counts>) -> StrangePlanet {
        StrangePlanet { states: counts.into_iter().map(|cs| State::new(cs)).collect() }
    }

    /// Returns a Graphviz DOT string representing this NFA.
    pub fn to_dot(&self) -> String {
        let mut dot = "digraph strange_planet {\n".to_string();
        dot.push_str("    rankdir = LR;\n");
        for state in &self.states {
            let shape = if state.forward.is_empty() {
                "doublecircle"
            } else {
                "circle"
            };
            let color = match state.failure {
                Failure::Guaranteed => "red",
                Failure::Possible => "darkgoldenrod",
                Failure::Impossible => "green",
            };
            dot.push_str(&format!("    node [shape = {}, color = {}]; ", shape, color));
            dot.push_str(&format!("\"{}\";\n", state.name));
            for target in state.forward(self) {
                dot.push_str(&format!("    \"{}\" -> \"{}\";\n", state.name, target.name));
            }
        }
        dot.push_str("}\n");
        dot
    }
}

impl ops::Index<usize> for StrangePlanet {
    type Output = State;

    fn index(&self, index: usize) -> &State {
        &self.states[index]
    }
}

impl ops::IndexMut<usize> for StrangePlanet {
    fn index_mut(&mut self, index: usize) -> &mut State {
        &mut self.states[index]
    }
}

//================================================
// Functions
//================================================

/// Generates all the possible species population counts.
fn generate_counts(species: usize, creatures: usize, left: usize) -> Vec<Counts> {
    if species == 1 {
        return vec![counts![creatures]];
    }

    let mut counts = vec![];
    let min = ((creatures as f64) / (species as f64)).ceil() as usize;
    let max = cmp::min(creatures, left);
    for first in (min..(max + 1)).rev() {
        for mut subcounts in generate_counts(species - 1, creatures - first, first) {
            subcounts.insert(0, first);
            counts.push(subcounts);
        }
    }
    counts
}

// Generates all the possible transitions.
fn generate_transitions(planet: &StrangePlanet) -> Vec<(usize, usize)> {
    // Generate a map from population counts to state indices.
    let states = planet.states.iter().enumerate().map(|(i, s)| {
        (&s.counts, i)
    }).collect::<HashMap<_, _>>();

    let mut transitions = vec![];
    let mut buffer = (0..planet.states.get(0).map_or(0, |s| s.counts.len())).collect::<Counts>();

    // Adds a transition from the supplied state using the supplied species if possible.
    macro_rules! generate {
        ($source:expr, $a:expr, $b:expr, $c:expr) => ({
            if $source.1[$a] > 0 && $source.1[$b] > 0 {
                buffer[..].copy_from_slice(&$source.1.counts);
                buffer[$a] -= 1;
                buffer[$b] -= 1;
                buffer[$c] += 2;
                buffer.sort_by(|a, b| b.cmp(a));
                transitions.push(($source.0, *states.get(&buffer).unwrap()));
            }
        })
    }

    // Generate all the possible transitions.
    for source in planet.states.iter().enumerate() {
        let counts = &source.1.counts;
        let max = counts.iter().position(|c| *c == 0).unwrap_or(counts.len());
        for a in 0..max {
            for b in (a + 1)..max {
                for c in (b + 1)..counts.len() {
                    generate!(source, a, b, c);
                    generate!(source, c, a, b);
                    generate!(source, b, c, a);
                }
            }
        }
    }

    transitions
}

/// Marks the states that may or must fail.
fn mark_failure(planet: &mut StrangePlanet, index: usize, seen: &mut HashSet<usize>) {
    seen.insert(index);

    // The current state must fail if all the states it can transition into also must fail.
    let guaranteed = planet[index].forward(planet).all(|s| s.failure == Failure::Guaranteed);
    planet[index].failure = if guaranteed {
        Failure::Guaranteed
    } else {
        Failure::Possible
    };

    // Visit all the unvisited states that can transition into the current state.
    for index in planet[index].back.clone().into_iter() {
        if !seen.contains(&index) {
            mark_failure(planet, index, seen);
        }
    }
}

/// Generates a strange planet NFA with the supplied number of species and creatures.
pub fn generate(species: usize, creatures: usize) -> StrangePlanet {
    if species == 0 {
        panic!("`species` is not greater than `0`");
    }

    // Generate all the possible states.
    let mut planet = StrangePlanet::new(generate_counts(species, creatures, creatures));

    // Generate all the possible transitions.
    for (source, destination) in generate_transitions(&planet) {
        planet[source].forward.insert(destination);
        planet[destination].back.insert(source);
    }

    // Mark the states that may or must fail.
    mark_failure(&mut planet, 0, &mut HashSet::new());

    planet
}
