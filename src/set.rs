use smallvec::{SmallVec};

/// A type used to store indices.
type Indices = SmallVec<[usize; 32]>;

/// A set of indices.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct IndexSet {
    set: Indices,
}

impl IndexSet {
    /// Constructs a new `IndexSet`.
    pub fn new() -> IndexSet {
        IndexSet { set: Indices::new() }
    }

    /// Returns whether this set is empty.
    pub fn is_empty(&self) -> bool {
        self.set.is_empty()
    }

    /// Returns whether the supplied index is in this set.
    pub fn contains(&self, index: usize) -> bool {
        self.set.binary_search(&index).is_ok()
    }

    /// Inserts the supplied index into this set.
    pub fn insert(&mut self, index: usize) {
        if let Err(position) = self.set.binary_search(&index) {
            self.set.insert(position, index);
        }
    }

    /// Returns an iterator over the indices in this set.
    pub fn iter<'a>(&'a self) -> impl Iterator<Item=&'a usize> + 'a {
        self.set.iter()
    }

    /// Returns an iterator over the indices in this set.
    pub fn into_iter(self) -> impl Iterator<Item=usize> {
        self.set.into_iter()
    }
}
